function result=global_fit(vector,options)

%Construct Predefined options
if isempty(options)
    options=struct('resampling',true,...
        'report',true,...
        'Nconstructs',5,...
        'model',1,...
        'mu',[0.0042,0.00379,0.00246,0.00539],...
        'de',0.09);
end

%Define output structure
result=struct('para100',[],'sigma',[]);

%Generate histgorams from event-vectors
for idx=1:numel(vector)
    %Generate Histogram
    [n{idx},p{idx}]=rnaPda.get_hist(vector{idx});
end

%Fitting of probability distributions with fmincon
options_fmincon=optimoptions('fmincon','Display','off');

%Construct Modeldep. Constraints
[A,b]=model2constraint(options);
de=1;
N=options.Nconstructs*3;

%Fit
[para100,d] = fmincon(@(para)histdist(para,p,de),...
    ones(1,N)*1,[],[],A,b,zeros(1,N),ones(1,N)*1000,...
    [],options_fmincon); 

%Adj. Rsquare
SSE=d;
SST=var(cell2mat(p));
numf=numel(cell2mat(p));
nump=numel(para100)+numel(b);
R=1-SSE/(SST*(numf-nump));

%Save
for idx=1:numel(p)
    idx0=(idx-1)*3+1;
    result.para100(idx,1:3)=para100(idx0:idx0+2);
end
result.d=d;

%Draw report figure
if options.report
    disp(para100)
    figure('Position',[400,400,500,250])
    for idx=1:numel(p)
        subplot(1,numel(p),idx)
        idx0=(idx-1)*3+1;
        para1=para100(idx0:idx0+2);
        [n1,p1]=pncalc(para1(1),para1(2),para1(3),de);
        hold on
        plot(n1,p1)
        plot(n{idx},p{idx})
        box on
        xlim([1 20])
        ylim([0 0.21])
    end
end

%Perform resampling
if options.resampling
    for idx=1:500
        %Generate histogram from resampled event-vectors
        for idx2=1:numel(vector)
            te=rnaPda.resampler(vector{idx2},0.80);
            %Generate Histogram
            [n_resamp{idx2},p_resamp{idx2}]=rnaPda.get_hist(te);
        end
        %Perform fit
        [para(idx,:),d(idx)] = fmincon(@(para)histdist(para,p_resamp,de),...
                        ones(1,N),[],[],A,b,zeros(1,N),ones(1,N)*100,...
                        [],options_fmincon); 
    end
    sigma=std(para);
    for idx=1:numel(p)
        idx0=(idx-1)*3+1;
        result.sigma(idx,:)=sigma(idx0:idx0+2);
    end
    result.sigmaB=std(para(:,3)./para(:,2));
else
    result.sigma=[];
    result.sigmaB=[];
end

end

function [d]=histdist(para,yall,de)
    d=0;
    for idx=1:numel(yall)
        ytarget=yall{idx};
        %Calculate distance between two distrinutions
        idx0=(idx-1)*3+1;
        para1=para(idx0:idx0+2);
        [~,p]=pncalc(para1(1),para1(2),para1(3),de);
        %Initialize d
        %Sum up residuals
        for q=1:max(numel(p),numel(ytarget))
           if and(q<=numel(p),q<=numel(ytarget)) 
              d=d+(p(q)-ytarget(q)).^2;
           end
        end 
    end
end     
            
function [A,b]=model2constraint(pa)
    %Genrate constraints for different two-state models
    switch pa.model
        case 1 
            %Constant koff and constant kTx, varying kon
            A=zeros(2*(pa.Nconstructs-1),pa.Nconstructs*3);
            b=zeros(2*(pa.Nconstructs-1),1);

            %Equal koff amongst all cosntructs
            for idx=1:(pa.Nconstructs-1)
               idx0=2+(idx-1)*3; 
               A(idx,idx0)=1;
               A(idx,idx0+3)=-1;
            end
            idx_start=(pa.Nconstructs-1);
            %Equal kTX amongst all cosntructs
            for idx=1:(pa.Nconstructs-1)
               idx0=3+(idx-1)*3; 
               A(idx+idx_start,idx0)=1;
               A(idx+idx_start,idx0+3)=-1;
            end             
            
        case 2
            %Constant kon and constant kTx, varying koff
            A=zeros(2*(pa.Nconstructs-1),pa.Nconstructs*3);
            b=zeros(2*(pa.Nconstructs-1),1);

            %Equal koff amongst all cosntructs
            for idx=1:(pa.Nconstructs-1)
               idx0=1+(idx-1)*3; 
               A(idx,idx0)=1;
               A(idx,idx0+3)=-1;
            end
            idx_start=(pa.Nconstructs-1);
            %Equal kTX amongst all cosntructs
            for idx=1:(pa.Nconstructs-1)
               idx0=3+(idx-1)*3; 
               A(idx+idx_start,idx0)=1;
               A(idx+idx_start,idx0+3)=-1;
            end
        case 3
            %Constant kon and constant koff, varying kTX
            A=zeros(2*(pa.Nconstructs-1),pa.Nconstructs*3);
            b=zeros(2*(pa.Nconstructs-1),1);

            %Equal koff amongst all cosntructs
            for idx=1:(pa.Nconstructs-1)
               idx0=1+(idx-1)*3; 
               A(idx,idx0)=1;
               A(idx,idx0+3)=-1;
            end
            idx_start=(pa.Nconstructs-1);
            %Equal kTX amongst all cosntructs
            for idx=1:(pa.Nconstructs-1)
               idx0=2+(idx-1)*3; 
               A(idx+idx_start,idx0)=1;
               A(idx+idx_start,idx0+3)=-1;
            end
    end
end

