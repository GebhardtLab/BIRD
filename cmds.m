
%Instance of rnaPda class
tale=rnaPda();

%Options for fitting
options=struct('resampling',false,...
        'report',true,...
        'Nconstructs',5,...
        'model',1,...
        'mu',[0.0042,0.00379,0.00246,0.00539],...
        'de',2.7799e-05*3600);

%%
for idx=1:3
    options.model=idx;
    result(idx)=tale.fit_expressions(options);
    d(idx)=result(idx).d;
    options.resampling=false;
end

figure
bar(d)

%Infer effective on-rate    
tale.response(result(1),options)




%options.model=1;
%options.resampling=false;
%result_conc_dep=tale.conc_dep_on_rate(options);
%tale.conc_dep_response(result_conc_dep,options)
