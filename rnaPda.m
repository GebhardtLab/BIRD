classdef rnaPda 
    %Rna hIsTogram Analysis

    properties
        report=true;
        expression
        name
        ntf
        tsite
        master_bins
        a_target
        activation_parameter=3./[150,200,260];
    end
    
    methods
        %------------------------------------------------------------------
        function obj = rnaPda()
            %TALE_ANALYSIS Construct an instance of this class
            %   Detailed explanation goes here
            load('data')
            %Save valiables
            obj.expression=expression;
            obj.name=name;
            obj.ntf=ntf;
            obj.tsite=tsite;
            
            %Predefined activation distribution
            bins=linspace(0,1000,21);
            obj.master_bins=bins+25;
            obj.a_target=[ones(1,4),zeros(1,17)]*1000;
        end
        %------------------------------------------------------------------
        function result=fit_expressions(obj,options)
            %Collect expression histograms
            %Empty cells
            vector{1}=obj.expression{1};
            %TALE constructs
            %figure
            %hold on
            %plot(obj.master_bins,obj.a_target)
            for q=2:5
                %[y,x]=hist(obj.ntf{q},linspace(0,1000,30));
                %plot(x,y)
                %Select expression levels according to distribution
                vector{q}=rnaPda.select_by_pdf(obj.ntf{q},obj.expression{q},...
                    obj.master_bins,obj.a_target);
            end
            %Perform Fit
            result=global_fit(vector,options);     
        end
        %------------------------------------------------------------------
        function []=response(obj,result,options)
            x=options.mu';
            y=1./(result.para100(2:end,1)-result.para100(1));
            y=1./(result.para100(2:end,1));
            y=y/options.de;
            
            disp('---pon---')
            disp(result.para100(:,1)./(result.para100(:,1)+result.para100(:,2)))
            disp('---pon---')
            
            disp('---Burstsize--')
            disp(result.para100(1,3)./result.para100(1,2))
            disp('---Burstsize---')
            
            figure
            hold on
            try
                sigma=sqrt(result.sigma(2:end,1).^2+result.sigma(1,1).^2)*options.de;
                errorbar(x,y,sigma.*y.^2,'o','MarkerFaceColor',[0 0.447058823529412 0.741176470588235])
            catch
                plot(x,y,'+')    
            end
            % Fit linear response
            [linear_fit, linear_gof]=fit( x, y, fittype( 'poly1' ));
            
            % Fit exponential response
            [exp_fit, exp_gof] = fit( x, y, fittype( 'exp1' ));

            % Plot fit with data.
            plot(linspace(2e-3,6e-3),linear_fit(linspace(2e-3,6e-3)),'k-')
            plot(linspace(2e-3,6e-3),exp_fit(linspace(2e-3,6e-3)),'r-')
            %disp(linear_gof)
            disp(exp_fit)
            ylabel('1/k_{on,eff} (h)')
            xlabel('k_{off TALE} (s^{-1})')
            box on
            xlim([2e-3 6e-3])
            ylim([0 60])
            legend('Data','Linear Fit','Exponential Fit')
        end 
        %------------------------------------------------------------------
        function []=conc_dep_response(obj,result_all,options)
            figure
            hold on
            xlim([0 4])
            
            for idx=2:numel(result_all)
                result=result_all{idx};
                x=(1:3)';
                y=result.para100(:,1);
                

                try
                    sigma=result.sigma(:,1);
                    errorbar(x,y,sigma,'+')  
                catch
                    plot(x,y,'+')    
                end
                % Fit linear response
                [linear_fit, linear_gof]=fit( x, y, fittype( 'poly1' ));

                % Plot fit with data.
                plot(linear_fit)
                disp(linear_gof)
            end
        end 
        %------------------------------------------------------------------
        function result=conc_dep_on_rate(obj,options)
            k=obj.activation_parameter;
            X=obj.master_bins;
            %iterate through constructs and concentrations
            for q=2:5
                for idx=1:numel(k)
                    a=X.^3.*exp(-X*k(idx));
                    a=round(a/sum(a)*200);
                    new_m=rnaPda.select_by_pdf(obj.ntf{q},obj.expression{q},...
                        X,a);
                    vector{idx}=new_m;
                end
                result{q}=global_fit(vector,options);
            end         
        end
    end
    
    methods(Static)
        %------------------------------------------------------------------
        function p=superposition(para,act_x,act_y,act_E)
            %Superposition of multiple activation levels
            p=zeros(1,1000);
            for q=1:numel(act_x)
                [n,p_1]=pncalc(para(1)/act_E*act_x(q),para(2),para(3),1e-2);
                p_1(p_1<0)=0;
                for idx=1:numel(n)
                   p(n(idx)+1)=p(n(idx)+1)+p_1(idx)*act_y(q); 
                end
            end
            p(p<1e-10)=[];
        end
        %------------------------------------------------------------------  
        function vector=select_by_pdf(n,m,x,y)
            %Select expression levels such that a given proability
            %distribution is filled up
            bin_delta=(x(2)-x(1))/2;
            vector=[];
            for q=1:numel(x)
                bin=[-bin_delta,bin_delta]+x(q);
                for w=1:y(q)
                    [n,m,val]=retrieve(n,m,bin);
                    vector=[vector,val];
                end
            end
            
            %***********************************
            function [n,m,val]=retrieve(n,m,bin)
                %retrieve values
                idx=find(and(bin(1)<n,bin(2)>n),1);
                if isempty(idx)
                    %disp('retrieving point failed')
                    val=[];
                else
                    val=m(idx);
                    m(idx)=[];
                    n(idx)=[];
                end
            end
            %***********************************
        end
        %------------------------------------------------------------------
        function [n,p]=get_hist(m)
            [p,n]=hist(m,(0:1:max(m)));
            p=p/sum(p);
        end
        %------------------------------------------------------------------
        function []=readata()
            %Read data from excel-sheet
            [file,path]=uigetfile();
            for ncond=1:5
                sheet=xlsread([path '/' file],ncond);
                %Normalized number of TFs
                ntf{ncond}=sheet(:,7);
                %RNA levels
                n=sheet(:,6);
                %Burst
                n_B{ncond}=sheet(:,5);
                n_B{ncond}(isnan(n_B{ncond}))=0;
                %Deleta Data with burst ocurring -> off Histogram
                %n(n_B>0)=[];
                %expression{ncond}=n+n_B;
                if ncond~=1
                   %ntf{ncond}(n_B>0)=[];
                   expression{ncond}=n;
                else
                    n_b=-sheet(:,4);
                    n_b(isnan(n_b))=0;
                    expression{ncond}=n;
                end
                %RNA at transcirption site
                tsite{ncond}=sheet(:,3);
            end

            %Names are predefined    
            name={'empty','9R','13R','15R','19R'};

            %Tidy up    
            clear ncond file path sheet n
            save('data')
        end
        %------------------------------------------------------------------      
        function resampled=resampler(vector,percentage)
            resampled=[];
            %Amount of samples
            Q=floor(numel(vector)*percentage);
            for idx=1:Q
                choice =ceil(rand*numel(vector));
                %Add event to new event vector
                resampled=[resampled,vector(choice)];
                %No replacement
                vector(choice)=[]; 
            end     
        end
        
    end
end

