function [n,p]=pncalc(la,mu,nu,de)
warning('off','MATLAB:nearlySingularMatrix')
warning('off','MATLAB:illConditionedMatrix')
warning('off','MATLAB:singularMatrix')

    %Normalize values
    la=la/de;mu=mu/de;nu=nu/de;

    %Two state model
    T=[-la,mu;la,-mu];
    Tg=[-la,mu;1,1];
    cg=Tg^-1*[0;1];
    E=3*ceil(cg(end)*nu);
    E=max(E,0);
    %Prepare Iteration
    rightside=cg;
    [V,D]=eig((E+1)^-1*m(T,1,nu));
    [~,minidx]=min(diag(D));
    vEalt=V(:,minidx)+1e-5*[1;-1];
    vEneu=V(:,minidx);

    %Iteration
    pend=1;q=1;
    while and(sum(abs(pend))>1e-4,not(q>100))
        q=q+1;
        nmax=E+q*1;
        start=[vEalt,vEneu];  
        [matrix,lastu]=getcs(T,nu,nmax,E,start);
        vEalt=vEneu;

        x=(matrix\rightside);
        pend=lastu*x;
        vEneu=start*x;
    end

    [n,p]=calcpwithmat(start,x,T,E,nu,nmax);

end

function [n,p]=calcpwithmat(start,x,T,E,nu,nmax)

    %Mapping p: p(n+1)->p(n) 
    n=0:1:nmax;

    %Expectation value    
    p(:,E+1)=sum(start*x);
    
    %Interation to maximum value
    u=start;
    for q=E:nmax-1
       u=1/(q+1)*(m(T,q,nu)*u);
       p(:,q+2)=sum(u*x);   
    end

    %Interation to minimum value
    u=start;
    for q=E:-1:1
       u=q*(m(T,q-1,nu)\u);
       p(:,q)=sum(u*x);  
    end

end

function [matrix,lastu]=getcs(T,nu,nmax,E,start)

    %Preallocate memory
    matrix=zeros(size(start));
    u=start;    
    cs=u;
    
    %Interation to maximum value
    for q=E:nmax-1
        u=1/(q+1)*(m(T,q,nu)*u);
        cs=cs+u;
    end   
    %Save Matrix elements
    matrix=u;
    lastu=u;
    
    %Interation to minimum value
    u=start;
    for q=E:-1:1
        u=(q)*(m(T,q-1,nu)\u);
        cs=cs+u;
    end 
    
    %Save Matrix elements
    for q=1:size(matrix,1)
        matrix(end,q)=cs(2,q);
    end
    
    matrix=cs;
    
end


function [M]=m(T,n,nu)
    %Set up matrix
    M=-T+diag(ones(1,size(T,1)))*n;
    M(end,end)=M(end,end)+nu;
    M(end,:)=M(end,:)-n;
end

% figure('Units','centimeters','Position',[7,7,20,13],'Papersize',[13 7],'Renderer','Painters');
% 
% stem(n,p','MarkerFaceColor',[0 0.447058826684952 0.74117648601532],...
%     'MarkerEdgeColor',[0 0.447058826684952 0.74117648601532],...
%     'MarkerSize',2);
% xlabel('n')
% ylabel('p(n)')

